﻿namespace HiWebForms.Models
{
    /// <summary>
    /// 食堂基本資訊
    /// </summary>
    public class Restaurant
    {
        /// <summary>
        /// 唉低
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 名稱
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 電話
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// 星數
        /// </summary>
        public float? Stars { get; set; }

        /// <summary>
        /// 評價數
        /// </summary>
        public int? Comments { get; set; }

        /// <summary>
        /// 標籤
        /// </summary>
        public string Tags { get; set; }
    }
}
