﻿namespace HiWebForms.Models
{
    /// <summary>
    /// 評價
    /// </summary>
    public class Review
    {
        /// <summary>
        /// 唉低
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 小夥伴唉低
        /// </summary>
        public int FellowId { get; set; }

        /// <summary>
        /// 食堂唉低
        /// </summary>
        public int RestaurantId { get; set; }

        /// <summary>
        /// 星數
        /// </summary>
        public float? Score { get; set; }

        /// <summary>
        /// 評價
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// 小夥伴
        /// </summary>
        public virtual Fellow Fellow { get; set; }

        /// <summary>
        /// 食堂
        /// </summary>
        public virtual Restaurant Restaurant { get; set; }
    }
}
