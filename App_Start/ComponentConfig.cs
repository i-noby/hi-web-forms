﻿using Component.Sql;
using System.Web;

[assembly: PreApplicationStartMethod(typeof(Component.Web.ComponentConfig), "Start")]

namespace Component.Web
{
    public class ComponentConfig
    {
        public static void Start()
        {
            SqlDbHelper.DefaultConnectionString =
                "Data Source=localhost\\SQLEXPRESS;Initial Catalog=Hi;Integrated Security=True";
        }
    }
}
