﻿<%@ Page Title="修改食堂" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="HiWebForms.Restaurants.Modify" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div class="form-horizontal">
        <hr>
        <div class="form-group">
            <label class="control-label col-md-2" for="Name">名稱</label>
            <div class="col-md-10">
                <asp:TextBox ID="NameTextBox" runat="server" class="form-control text-box single-line"></asp:TextBox>                
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="Address">地址</label>
            <div class="col-md-10">
                <asp:TextBox ID="AddressTextBox" runat="server" class="form-control text-box single-line"></asp:TextBox>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="PhoneNumber">電話</label>
            <div class="col-md-10">
                <asp:TextBox ID="PhoneNumberTextBox" runat="server" class="form-control text-box single-line"></asp:TextBox>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2" for="Tags">標籤</label>
            <div class="col-md-10">
                <asp:TextBox ID="TagsTextBox" runat="server" class="form-control text-box single-line"></asp:TextBox>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="CreateButton" runat="server" Text="儲存啦，哪次不儲存" class="btn btn-default" OnClick="ModifyButton_Click" />
            </div>
        </div>
    </div>
    <div>
        <a runat="server" href="~/Restaurants">回頭是岸</a>
    </div>
</asp:Content>
