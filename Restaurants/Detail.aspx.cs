﻿using HiWebForms.Controllers;
using HiWebForms.Models;
using System;

namespace HiWebForms.Restaurants
{
    public partial class Detail : System.Web.UI.Page
    {
        private readonly RestaurantsController restaurantsController = new RestaurantsController();

        public Restaurant restaurant;

        protected void Page_Load(object sender, EventArgs e)
        {
            var hasId = int.TryParse(Request.QueryString["id"], out var id);
            if (!hasId)
            {
                Response.Redirect("~/Restaurants");
            }
            restaurant = restaurantsController.Detail(id);
        }
    }
}
