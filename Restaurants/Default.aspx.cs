﻿using HiWebForms.Controllers;
using HiWebForms.Models;
using System;
using System.Collections.Generic;

namespace HiWebForms.Restaurants
{
    public partial class Default : System.Web.UI.Page
    {
        private readonly RestaurantsController restaurantsController = new RestaurantsController();

        public List<Restaurant> restaurants;

        protected void Page_Load(object sender, EventArgs e)
        {
            restaurants = restaurantsController.Query(Request.QueryString["name"]);
        }
    }
}
