﻿using HiWebForms.Controllers;
using HiWebForms.Models;
using System;

namespace HiWebForms.Restaurants
{
    public partial class Create : System.Web.UI.Page
    {
        private readonly RestaurantsController restaurantsController = new RestaurantsController();

        protected void Page_Load(object sender, EventArgs e) { }

        protected void CreateButton_Click(object sender, EventArgs e)
        {
            var restaurant = new Restaurant
            {
                Name = NameTextBox.Text,
                Address = AddressTextBox.Text,
                PhoneNumber = PhoneNumberTextBox.Text,
                Tags = TagsTextBox.Text
            };
            restaurantsController.Create(restaurant);
            Response.Redirect("~/Restaurants");
        }
    }
}
