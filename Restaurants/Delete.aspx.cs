﻿using HiWebForms.Controllers;
using HiWebForms.Models;
using System;

namespace HiWebForms.Restaurants
{
    public partial class Delete : System.Web.UI.Page
    {
        private readonly RestaurantsController restaurantsController = new RestaurantsController();

        public Restaurant restaurant;

        public int id = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            var hasId = int.TryParse(Request.QueryString["id"], out id);
            if (!hasId)
            {
                Response.Redirect("~/Restaurants");
            }
            if (!IsPostBack)
            {
                restaurant = restaurantsController.Detail(id);
            }
        }

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            restaurantsController.Delete(id);
            Response.Redirect("~/Restaurants");
        }
    }
}
