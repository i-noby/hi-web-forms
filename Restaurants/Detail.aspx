﻿<%@ Page Title="食堂詳情" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Detail.aspx.cs" Inherits="HiWebForms.Restaurants.Detail" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div>
        <hr />
        <dl class="dl-horizontal">
            <dt>名稱</dt>
            <dd><%: restaurant.Name %></dd>
            <dt>地址</dt>
            <dd><%: restaurant.Address %></dd>
            <dt>電話</dt>
            <dd><%: restaurant.PhoneNumber %></dd>
            <dt>星數</dt>
            <dd><%: restaurant.Stars %></dd>
            <dt>評價數</dt>
            <dd><%: restaurant.Comments %></dd>
            <dt>標籤</dt>
            <dd><%: restaurant.Tags %></dd>
        </dl>
    </div>
    <p>
        <a href="<%= ResolveUrl("~/Restaurants/Modify?id=" + restaurant.Id) %>">編輯</a> |
        <a runat="server" href="~/Restaurants">回頭是岸</a>
    </p>
</asp:Content>
