﻿<%@ Page Title="食堂詳情" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Delete.aspx.cs" Inherits="HiWebForms.Restaurants.Delete" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div>
        <hr />
        <dl class="dl-horizontal">
            <dt>名稱</dt>
            <dd><%: restaurant.Name %></dd>
            <dt>地址</dt>
            <dd><%: restaurant.Address %></dd>
            <dt>電話</dt>
            <dd><%: restaurant.PhoneNumber %></dd>
            <dt>星數</dt>
            <dd><%: restaurant.Stars %></dd>
            <dt>評價數</dt>
            <dd><%: restaurant.Comments %></dd>
            <dt>標籤</dt>
            <dd><%: restaurant.Tags %></dd>
        </dl>
    </div>
    <p>
        <asp:Button ID="DeleteButton" runat="server" Text="拋棄啦，哪次不拋棄" class="btn btn-default" OnClick="DeleteButton_Click" /> |
        <a runat="server" href="~/Restaurants">回頭是岸</a>
    </p>
</asp:Content>
