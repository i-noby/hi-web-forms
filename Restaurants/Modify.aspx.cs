﻿using HiWebForms.Controllers;
using HiWebForms.Models;
using System;

namespace HiWebForms.Restaurants
{
    public partial class Modify : System.Web.UI.Page
    {
        private readonly RestaurantsController restaurantsController = new RestaurantsController();

        public Restaurant restaurant;

        public int id = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            var hasId = int.TryParse(Request.QueryString["id"], out id);
            if (!hasId)
            {
                Response.Redirect("~/Restaurants");
            }
            if (!IsPostBack)
            {
                restaurant = restaurantsController.Detail(id);
                NameTextBox.Text = restaurant.Name;
                AddressTextBox.Text = restaurant.Address;
                PhoneNumberTextBox.Text = restaurant.PhoneNumber;
                TagsTextBox.Text = restaurant.Tags;
            }
        }

        protected void ModifyButton_Click(object sender, EventArgs e)
        {
            var restaurant = new Restaurant
            {
                Id = id,
                Name = NameTextBox.Text,
                Address = AddressTextBox.Text,
                PhoneNumber = PhoneNumberTextBox.Text,
                Tags = TagsTextBox.Text
            };
            restaurantsController.Modify(restaurant);
            Response.Redirect("~/Restaurants");
        }
    }
}
