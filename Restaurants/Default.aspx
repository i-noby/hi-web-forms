﻿<%@ Page Title="食堂列表" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HiWebForms.Restaurants.Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
  <a
    runat="server"
    href="~/Restaurants/Create"
    class="btn btn-primary pull-right"
    style="margin-top: 20px"
    >新增食堂
  </a>
  <h2><%: Title %></h2>
  <hr />
  <% if (restaurants.Count > 0) { %>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>名稱</th>
        <th>地址</th>
        <th>電話</th>
        <th>星數</th>
        <th>評價數</th>
        <th>標籤</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <% foreach (var restaurant in restaurants) { %>
      <tr>
        <td><%= restaurant.Name %></td>
        <td><%= restaurant.Address %></td>
        <td><%= restaurant.PhoneNumber %></td>
        <td><%= restaurant.Stars %></td>
        <td><%= restaurant.Comments %></td>
        <td><%= restaurant.Tags %></td>
        <td>
            <a href="<%= ResolveUrl("~/Restaurants/Modify?id=" + restaurant.Id) %>">編輯</a> |
            <a href="<%= ResolveUrl("~/Restaurants/Detail?id=" + restaurant.Id) %>">詳情</a> |
            <a href="<%= ResolveUrl("~/Restaurants/Delete?id=" + restaurant.Id) %>">刪除</a>
        </td>
      </tr>
      <% } %>
    </tbody>
  </table>
  <% } else { %>
  <h3>查無資料</h3>
  <% } %>
</asp:Content>
