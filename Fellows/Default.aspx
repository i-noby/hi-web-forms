﻿<%@ Page Title="小夥伴列表" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HiWebForms.Fellows.Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
  <h2><%: Title %></h2>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
      </tr>
    </thead>
    <tbody>
      <% foreach (var item in list) { %>
      <tr>
        <td><%= item.Id %></td>
        <td><%= item.Name %></td>
      </tr>
      <% } %>
    </tbody>
  </table>
</asp:Content>
