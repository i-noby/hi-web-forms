﻿using Component.Sql;
using HiWebForms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace HiWebForms.Fellows
{
    public partial class Default : System.Web.UI.Page
    {
        public List<Fellow> list;

        protected void Page_Load(object sender, EventArgs e)
        {
            list = SqlDbHelper.GetEnumerable<Fellow>("SELECT * FROM Fellows").ToList();
        }
    }
}
