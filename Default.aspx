﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HiWebForms._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>Hi Web Forms</h1>
        <p class="lead">
            Web Forms 是微軟偉大的黑魔法發明，<br />
            他讓你擁有類 Windows Forms 的開發體驗，<br />
            拉一拉控制項就完成動態網頁的超能力。
        </p>
        <p><a href="javascript:alert('no more')" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>
</asp:Content>
