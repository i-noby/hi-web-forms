﻿using Component.Sql;
using HiWebForms.Models;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace HiWebForms.Controllers
{
    public class RestaurantsController
    {
        public List<Restaurant> Query(string name)
        {
            var parameters = new List<SqlParameter>();
            var sql = "SELECT * FROM Restaurants";
            if (!string.IsNullOrEmpty(name))
            {
                sql += " WHERE Name LIKE '%' + @Name + '%'";
                parameters.Add(new SqlParameter("Name", name));
            }
            var restaurants = SqlDbHelper.GetEnumerable<Restaurant>(sql, parameters).ToList();
            return restaurants;
        }

        public Restaurant Detail(int id)
        {
            var sql = "SELECT * FROM Restaurants WHERE Id = @Id";
            var parameter = new SqlParameter("Id", id);
            var restaurant = SqlDbHelper.GetElement<Restaurant>(sql, parameter);
            return restaurant;
        }

        public int Create(Restaurant restaurant)
        {
            var sql =
                @"
INSERT INTO Restaurants
           (Name
           ,Address
           ,PhoneNumber
           ,Stars
           ,Comments
           ,Tags)
     VALUES
           (@Name
           ,@Address
           ,@PhoneNumber
           ,0
           ,0
           ,@Tags)";
            return SqlDbHelper.NonQuery(
                sql,
                new SqlParameter("Name", restaurant.Name),
                new SqlParameter("Address", restaurant.Address),
                new SqlParameter("PhoneNumber", restaurant.PhoneNumber),
                new SqlParameter("Tags", restaurant.Tags)
            );
        }

        public int Modify(Restaurant restaurant)
        {
            var sql =
                @"
UPDATE Restaurants
   SET Name = @Name
      ,Address = @Address
      ,PhoneNumber = @PhoneNumber
      ,Tags = @Tags
 WHERE Id = @Id";
            return SqlDbHelper.NonQuery(
                sql,
                new SqlParameter("Name", restaurant.Name),
                new SqlParameter("Address", restaurant.Address),
                new SqlParameter("PhoneNumber", restaurant.PhoneNumber),
                new SqlParameter("Tags", restaurant.Tags),
                new SqlParameter("Id", restaurant.Id)
            );
        }

        public int Delete(int id)
        {
            var sql =
                @"
DELETE FROM Restaurants
 WHERE Id = @Id";
            return SqlDbHelper.NonQuery(sql, new SqlParameter("Id", id));
        }
    }
}
